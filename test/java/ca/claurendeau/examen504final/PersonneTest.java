package ca.claurendeau.examen504final;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersonneTest {
	Context context;
	final String ceQueUnePersonneHeureuseDevraitDire = "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour �tre une personne heureuse!";
	final String ceQueUnePersonneMalheureuseDevraitDire = "J'ai besoin d'un MacBook Pro pour �tre une personne heureuse!";
 	final String ceQueUnePersonneTristeDevraitDire = "Je fais parti des gens qui n'auront jamais de MacBook Pro";
	
	@Before
	public void setUp() throws Exception {
		context = new Context();
	}

	@After
	public void tearDown() throws Exception {
		context = null;
	}

	@Test
	public void testPersonneHEUREUSE() {
	    HeureuseState heureuseState = new HeureuseState();
	    heureuseState.doAction(context);
	    assertEquals(ceQueUnePersonneHeureuseDevraitDire, context.getState().toString());
	}
	
	@Test
	public void testPersonneMALHEUREUSE() {
		MalheureuseState malheureuseState = new MalheureuseState();
	    malheureuseState.doAction(context);
	    assertEquals(ceQueUnePersonneMalheureuseDevraitDire, context.getState().toString());
	}
	
	@Test
	public void testPersonneTRISTE() {
		TristeState tristeState = new TristeState();
	    tristeState.doAction(context);
	    assertEquals(ceQueUnePersonneTristeDevraitDire, context.getState().toString());
	}
}
