package ca.claurendeau.examen504final;
public interface State {
   public void doAction(Context context);
}