package ca.claurendeau.examen504final;

public class PersonneService {
    public static void main(String[] args) {
    	Context context = new Context();
    	
    	createHeureusePersonne(context);
	    
	    createMalheureusePersonne(context);
	    
	    createTristePersonne(context);
    }

	private static void createTristePersonne(Context context) {
		TristeState tristeState = new TristeState();
	    tristeState.doAction(context);
	    System.out.println(context.getState().toString());
	}

	private static void createMalheureusePersonne(Context context) {
		MalheureuseState malheureuseState = new MalheureuseState();
	    malheureuseState.doAction(context);
	    System.out.println(context.getState().toString());
	}

	private static void createHeureusePersonne(Context context) {
		HeureuseState heureuseState = new HeureuseState();
	    heureuseState.doAction(context);
	    System.out.println(context.getState().toString());
	}
}
