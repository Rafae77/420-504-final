package ca.claurendeau.examen504final;

public class MalheureuseState implements State {
	final private String humeur = "malheureuse";

	@Override
	public void doAction(Context context) {
		System.out.println("Personne [humeur=" + humeur + "]");
		context.setState(this);
	}
	
	public String toString(){
	      return "J'ai besoin d'un MacBook Pro pour �tre une personne heureuse!";
	}

}
