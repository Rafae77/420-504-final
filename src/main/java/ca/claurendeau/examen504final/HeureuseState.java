package ca.claurendeau.examen504final;

public class HeureuseState implements State {
	final private String humeur = "heureuse";
	
	@Override
	public void doAction(Context context) {
		System.out.println("Personne [humeur=" + humeur + "]");
		context.setState(this);
	}
	
	public String toString(){
	      return "J'ai un MacBook Pro, j'ai tout ce qu'il me faut  pour �tre une personne heureuse!";
	}

}
