package ca.claurendeau.examen504final;

public class TristeState implements State {
	final private String humeur = "triste";

	@Override
	public void doAction(Context context) {
		System.out.println("Personne [humeur=" + humeur + "]");
		context.setState(this);
	}
	
	public String toString(){
	      return "Je fais parti des gens qui n'auront jamais de MacBook Pro";
	}
}
